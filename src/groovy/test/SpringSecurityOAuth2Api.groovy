package test

import org.apache.commons.logging.Log;
import java.util.regex.Matcher
import java.util.regex.Pattern

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.scribe.builder.api.DefaultApi20
import org.scribe.exceptions.OAuthException
import org.scribe.extractors.AccessTokenExtractor
import org.scribe.model.OAuthConfig
import org.scribe.model.OAuthConstants
import org.scribe.model.OAuthRequest
import org.scribe.model.Response
import org.scribe.model.Token
import org.scribe.model.Verb
import org.scribe.model.Verifier
import org.scribe.oauth.OAuth20ServiceImpl
import org.scribe.oauth.OAuthService
import org.scribe.utils.OAuthEncoder
import org.scribe.utils.Preconditions
/**
 *
 * @author bsaville
 */
public class SpringSecurityOAuth2Api extends DefaultApi20 {
  static final Log log = LogFactory.getLog(SpringSecurityOAuth2Api.class)
  private static final String AUTHORIZE_URL = "http://localhost:7676/OAuth2Demo/oauth/authorize?response_type=code&client_id=%s&redirect_uri=%s";
  private static final String SCOPED_AUTHORIZE_URL = AUTHORIZE_URL + "&scope=%s";
  private static final String SUFFIX_OFFLINE = "&access_type=offline&approval_prompt=force";
  private boolean offline = false;
  
	@Override
	public String getAccessTokenEndpoint() {
		return "http://localhost:7676/OAuth2Demo/oauth/token";
	}
  
  @Override
    public AccessTokenExtractor getAccessTokenExtractor() {
        return new AccessTokenExtractor() {
            @Override
            public Token extract(String response) {
                Preconditions.checkEmptyString(response, "Response body is incorrect. Can't extract a token from an empty string");

                Matcher matcher = Pattern.compile("\"access_token\" : \"([^&\"]+)\"").matcher(response);
                if (matcher.find()) {
                    String token = OAuthEncoder.decode(matcher.group(1));
                    String refreshToken = "";
                    Matcher refreshMatcher = Pattern.compile("\"refresh_token\" : \"([^&\"]+)\"").matcher(response);
                    if (refreshMatcher.find())
                        refreshToken = OAuthEncoder.decode(refreshMatcher.group(1));
                    Date expiry = null;
                    Matcher expiryMatcher = Pattern.compile("\"expires_in\" : ([^,&\"]+)").matcher(response);
                    if (expiryMatcher.find()) {
                        int lifeTime = Integer.parseInt(OAuthEncoder.decode(expiryMatcher.group(1)));
                        expiry = new Date(System.currentTimeMillis() + lifeTime * 1000);
                    }
                    return new Token(token, refreshToken, response);
                } else {
                    throw new OAuthException("Response body is incorrect. Can't extract a token from this: '" + response + "'", null);
                }
            }
        };

    }

    @Override
    public String getAuthorizationUrl(OAuthConfig config) {
        // Append scope if present
        if (config.hasScope()) {
            return String.format(offline ?
                            SCOPED_AUTHORIZE_URL + SUFFIX_OFFLINE : SCOPED_AUTHORIZE_URL, config.getApiKey(),
                    OAuthEncoder.encode(config.getCallback()),
                    OAuthEncoder.encode(config.getScope())
            );
        } else {
            return String.format(offline ?
                            AUTHORIZE_URL + SUFFIX_OFFLINE : AUTHORIZE_URL, config.getApiKey(),
                    OAuthEncoder.encode(config.getCallback())
            );
        }
    }

    @Override
    public Verb getAccessTokenVerb() {
        return Verb.GET;
    }

    @Override
    public OAuthService createService(OAuthConfig config) {
        return new Service(this, config);
    }

    public class Service extends OAuth20ServiceImpl {
      static final Log log = LogFactory.getLog(Service.class)
        private static final String GRANT_TYPE_AUTHORIZATION_CODE = "authorization_code";
        private static final String GRANT_TYPE = "grant_type";
        private DefaultApi20 api;
        private OAuthConfig config;

        public Service(DefaultApi20 api, OAuthConfig config) {
            super(api, config);
            this.api = api;
            this.config = config;
        }

      

        public boolean isOffline() {
            return offline;
        }

        @Override
        public Token getAccessToken(Token requestToken, Verifier verifier) {
            OAuthRequest request = new OAuthRequest(api.getAccessTokenVerb(), api.getAccessTokenEndpoint());
            switch (api.getAccessTokenVerb()) {
                case Verb.POST:
//                    request.addBodyParameter(OAuthConstants.CLIENT_ID, config.getApiKey());
//                    request.addBodyParameter(OAuthConstants.CLIENT_SECRET, config.getApiSecret());
//                    request.addBodyParameter(OAuthConstants.CODE, verifier.getValue());
//                    request.addBodyParameter(OAuthConstants.REDIRECT_URI, config.getCallback());
//                    if (config.hasScope()) request.addBodyParameter(OAuthConstants.SCOPE, config.getScope());
//                    request.addBodyParameter(GRANT_TYPE, GRANT_TYPE_AUTHORIZATION_CODE);
                    request.addHeader('Authorization',"Basic ${config.getApiSecret()}")
                    request.addQuerystringParameter(OAuthConstants.CLIENT_ID, config.getApiKey());
                    request.addQuerystringParameter(GRANT_TYPE, GRANT_TYPE_AUTHORIZATION_CODE);
                    request.addQuerystringParameter(OAuthConstants.CLIENT_SECRET, config.getApiSecret());
                    request.addQuerystringParameter(OAuthConstants.CODE, verifier.getValue());
                    request.addQuerystringParameter(OAuthConstants.REDIRECT_URI, config.getCallback());
                    if (config.hasScope()) request.addQuerystringParameter(OAuthConstants.SCOPE, config.getScope());
                    break;
                case Verb.GET:
                default:
                request.addHeader('Authorization',"Basic ${config.getApiSecret()}")
                    request.addQuerystringParameter(OAuthConstants.CLIENT_ID, config.getApiKey());
                    request.addQuerystringParameter(GRANT_TYPE, GRANT_TYPE_AUTHORIZATION_CODE);
                    request.addQuerystringParameter(OAuthConstants.CLIENT_SECRET, config.getApiSecret());
                    request.addQuerystringParameter(OAuthConstants.CODE, verifier.getValue());
                    request.addQuerystringParameter(OAuthConstants.REDIRECT_URI, config.getCallback());
                    if (config.hasScope()) request.addQuerystringParameter(OAuthConstants.SCOPE, config.getScope());
            }
            log.info "${config.getApiSecret()}"
            log.info "request: "+request
            Response response = request.send();
            log.info response
            if(response.code != 200){
              log.error "Failed to get AccessToken ${response.code}"
            }
            //  throw new RuntimeException("Failed to get AccessToken: %{response.getBody()}")
            //}
            return api.getAccessTokenExtractor().extract(response.getBody());
        }
    }

}