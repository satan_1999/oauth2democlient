grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
grails.server.port.http = 7677
//grails.project.war.file = "target/${appName}-${appVersion}.war"

// uncomment (and adjust settings) to fork the JVM to isolate classpaths
//grails.project.fork = [
//   run: [maxMemory:1024, minMemory:64, debug:false, maxPerm:256]
//]

grails.project.dependency.resolution = {
  // inherit Grails' default dependencies
  inherits("global") {
    // specify dependency exclusions here; for example, uncomment this to disable ehcache:
    // excludes 'ehcache'
  }
  log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
  checksums true // Whether to verify checksums on resolve
  legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

  repositories {
    inherits true // Whether to inherit repository definitions from plugins

    grailsPlugins()
    grailsHome()
    grailsCentral()

    mavenLocal()
    mavenCentral()
    mavenRepo "http://central.maven.org/maven2/"
    mavenRepo "http://repository.codehaus.org/"
    mavenRepo "http://central.maven.org/maven2/"
    mavenRepo "http://repo.spring.io/milestone/"
    // uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
    //mavenRepo "http://snapshots.repository.codehaus.org"
    //mavenRepo "http://repository.codehaus.org"
    //mavenRepo "http://download.java.net/maven/2/"
    //mavenRepo "http://repository.jboss.com/maven2/"
  }

  dependencies {
    // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.
    compile('org.codehaus.groovy.modules.http-builder:http-builder:0.5.2')
    compile('com.google.appengine:appengine-api-1.0-sdk:1.3.1')
    
    // runtime 'mysql:mysql-connector-java:5.1.22'
   /* compile('org.codehaus.groovy.modules.http-builder:http-builder:0.7.1') { excludes "commons-logging",  "groovy" }
    compile 'log4j:log4j:1.2.17',
        'commons-httpclient:commons-httpclient:3.1',
        'commons-net:commons-net:3.0.1',
        'org.codehaus.jackson:jackson-mapper-asl:1.9.8',
        'org.codehaus.jackson:jackson-core-asl:1.9.8',
        "joda-time:joda-time:2.1"
    runtime "net.sourceforge.jtds:jtds:1.2.4","org.jasypt:jasypt:1.7.1"
    compile "org.apache.httpcomponents:httpclient:4.3.2", { export = false }*/
  }

  plugins {
    runtime ":hibernate:$grailsVersion"
    runtime ":jquery:1.8.3"
    runtime ":resources:1.2"
    runtime ":console:1.3"
    // Uncomment these (or add new ones) to enable additional resources capabilities
    //runtime ":zipped-resources:1.0"
    //runtime ":cached-resources:1.0"
    //runtime ":yui-minify-resources:0.1.5"
    build ":tomcat:$grailsVersion"

    runtime ":database-migration:1.3.2"
    compile ":oauth:2.1.0"
  }
}
