package test

import groovyx.net.http.HTTPBuilder

import static groovyx.net.http.ContentType.TEXT
import static groovyx.net.http.Method.GET
import grails.converters.JSON
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder

import org.scribe.model.Token
import org.scribe.model.Verifier
import uk.co.desirableobjects.oauth.scribe.OauthService
import com.google.appengine.api.urlfetch.*
class TestController {
  OauthService oauthService
  private static final Token EMPTY_TOKEN = new Token('', '')
  def grailsApplication
  def index() {
    log.info("calling index ${params}")
  }

  def verify() {
    log.info("calling verify ${params}")
    Verifier verifier = new Verifier(params.code)

    log.info "verifier : ${verifier}"
    def accessToken = post(params.code)
    // Token accessToken = oauthService.getMineAccessToken(EMPTY_TOKEN, verifier)

    log.info "token : ${accessToken}"
    
    session.accessToken = accessToken
    flash.message = g.message(code: 'accesstoken.granted.message')
    render(view: "index")
  }
  
  def listBooks(){
    log.info("calling listBooks ${params}")
    def booksInJson = getResource(session.accessToken)
    //render html:booksInJson
    render(text:booksInJson,contentType:"text/json",encoding:"UTF-8")
  }
  
  private def getResource(def accessToken){
    log.info("calling getResource ${accessToken}")
    def data
      //def data = "http://localhost:7676/oauth2demo/book/list.json".toURL().getText(requestProperties: [Accept: 'application/json', Authorization:accessToken])
    def http = new HTTPBuilder( "http://localhost:7676/OAuth2Demo/book/list" )
    http.request(groovyx.net.http.Method.GET,ContentType.TEXT) { req ->
      headers.Accept = 'application/json'
      headers.Authorization = "Bearer ${accessToken}"
      uri.query = [format:'json']
      response.success = { resp, reader ->
        log.debug  resp.statusLine.statusCode
        log.debug "Got response: ${resp.statusLine}"
        log.debug "Content-Type: ${resp.headers.'Content-Type'}"
        data = reader.text
      }
    
      response.failure = { resp ->  
        log.error "*********Unexpected failure: ${resp.statusLine}" 
        data = [error : resp.statusLine.reasonPhrase, code : resp.statusLine.statusCode] as JSON
        }
    
    }
    log.info data
      return data
  }

  private def post(auCode){
    def result
    def client_id = grailsApplication.config.oauth.providers.mine.key as String
    def client_secret = grailsApplication.config.oauth.providers.mine.secret as String
    def redirect_uri = grailsApplication.config.oauth.providers.mine.callback as String
    def scope = grailsApplication.config.oauth.providers.mine.scope as String
    def http = new HTTPBuilder( grailsApplication.config.oauthProvider.url.toString() )
    def parameters = [ response_type:'code',client_id:client_id,redirect_uri:redirect_uri,grant_type:'authorization_code',scope:scope,code: auCode]
    //def parameters = [ response_type:'code',redirect_uri:redirect_uri,grant_type:'authorization_code',scope:scope,code: auCode]
    //def parameters = [ client_secret : client_secret, response_type:'code',client_id:client_id,redirect_uri:redirect_uri,grant_type:'authorization_code',scope:scope,code: auCode]
    http.request(groovyx.net.http.Method.POST,ContentType.TEXT) { req ->
      // uri.path = '/oauth/token' // overrides any path in the default URL
      headers.Accept = 'application/json'
      //headers.Authorization = client_secret
      //headers.Authorization = "Basic ${client_id}:${client_secret}"
      //headers.Authorization = "Basic ${client_id}:${client_secret}""".bytes.encodeBase64().toString()
      //uri.query = [response_type:'code',client_id:'my-client',redirect_uri:'http://localhost:7677/OAuth2DemoClient/test/verify',grant_type:'authorization_code',scope:'read',code: auCode]
      uri.query = parameters
      //body = parameters
      response.success = { resp, reader ->
        log.debug  resp.statusLine.statusCode
        log.debug "Got response: ${resp.statusLine}"
        log.debug "Content-Type: ${resp.headers.'Content-Type'}"
        result = reader.text
      }

      response.failure = { resp ->  log.error "*********Unexpected failure: ${resp.statusLine}" }
    }

    JSON.parse(result)?.access_token
  }

 
}
